# custom-questionnaire

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

# Coding challenge - Questionnaire
It is important to use the following technologies: 
- VueJS ^2
- TypeScript
- webpack
- yarn or npm
- SCSS with flex
- BEM
- Jest

Please style the components yourself and don't use bootstrap or any other styling frameworks.

Note: The resulting app does not have to be picture perfect.(styling wise)

# Todos
The overall idea is to build a VueJS frontend app that will read json data (see "questions.json") and dynamically build a questionnaire based on this data.

- There are 5 different types of questions: rating, age, email, password and comment.
- Once a user answers a question, a "result widget" appears on to top right with all the given questions and answers.
- Each question can have a sub-question which appears based on a value given to its parent question (see "design/2.png").
  for example: sub1 appears if parent question was answered with values 1-2 or sub2 appears when parent question was answered with values 4-5 and nothing appears for value 3.
- On the mobile design:
  - The result widget is not shown in the mobile view.
  - The age question is split into multiple lines. See "design/3.png".
- Validation rules for the form:
   - The field `"required"` from the example json data must be taken into account.
   - Password must contain at least one lowercase letter and a number.
   - Email question must pass a simple validation.
- On Submit, show either "success" or "not valid" on the page.
- Write automated tests with Jest 
- Provide steps to build the application in a Readme file.
- Upload the project on github, gitlab or bitbucket and send it to us. You can use a temporary or throwaway account to maintain privacy.

Happy coding!
