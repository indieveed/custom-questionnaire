import { Answer, QuestionOptions } from '@/components/questions/types'

export default function areRequiredFilled(
  questions: QuestionOptions[],
  answers: { [id: number]: Answer },
): boolean {
  const isRequiredMissing = questions.some((q) => {
    if (q.validation.required && !answers[q.id]) {
      return true
    }
    if (q.sub_questions) {
      return q.sub_questions.some((sq) => {
        // check only subquestions for current answer
        if (!sq.values.includes(answers[q.id].value as string)) return false
        return !areRequiredFilled(sq.questions, answers)
      })
    }
    return false
  })
  return !isRequiredMissing
}
