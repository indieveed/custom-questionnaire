import areRequiredFilled from '..'
import { QuestionOptions, Answer } from '../../components/questions/types'

describe('areRequiredFilled', () => {
  it('returns true with empty arguments', () => {
    expect(areRequiredFilled([], {})).toBe(true)
  })
  describe('without subquestions', () => {
    const questions = [
      {
        id: 1,
        validation: {
          required: true,
        },
      } as QuestionOptions,
      {
        id: 2,
        validation: {
          required: true,
        },
      } as QuestionOptions,
      {
        id: 3,
        validation: {
          required: false,
        },
      } as QuestionOptions,
      {
        id: 4,
        validation: {
          required: true,
        },
      } as QuestionOptions,
    ]

    it('returns true if all required questions are in answers', () => {
      const answers = {
        1: {
          id: 1,
        } as Answer,
        2: {
          id: 2,
        } as Answer,
        4: {
          id: 4,
        } as Answer,
      }
      const result = areRequiredFilled(questions, answers)
      expect(result).toBe(true)
    })
    it('returns false if a required question is not in answers', () => {
      const answers = {
        1: {
          id: 1,
        } as Answer,
        2: {
          id: 2,
        } as Answer,
      }
      const result = areRequiredFilled(questions, answers)
      expect(result).toBe(false)
    })
    it('returns true if there is no required questions', () => {
      const noRequiredQuestions = [
        {
          id: 1,
          validation: {
            required: false,
          },
        } as QuestionOptions,
        {
          id: 2,
          validation: {
            required: false,
          },
        } as QuestionOptions,
      ]
      const answers = {
      }
      const result = areRequiredFilled(noRequiredQuestions, answers)
      expect(result).toBe(true)
    })
    it('returns true if there is no validations', () => {
      const noRequiredQuestions = [
        {
          id: 1,
          validation: {
          },
        } as QuestionOptions,
        {
          id: 2,
          validation: {
          },
        } as QuestionOptions,
      ]
      const answers = {
      }
      const result = areRequiredFilled(noRequiredQuestions, answers)
      expect(result).toBe(true)
    })
  })

  describe('with subquestions', () => {
    const questions = [
      {
        id: 1,
        validation: {
          required: true,
        },
      } as QuestionOptions,
      {
        id: 2,
        validation: {
          required: true,
        },
        sub_questions: [
          {
            values: ['1', '2'],
            questions: [
              {
                id: 10,
                validation: {
                  required: true,
                },
              },
            ],
          },
          {
            values: ['3', '4'],
            questions: [
              {
                id: 11,
                validation: {
                  required: true,
                },
              },
            ],
          },
        ],
      } as QuestionOptions,
      {
        id: 3,
        validation: {
          required: false,
        },
      } as QuestionOptions,
      {
        id: 4,
        validation: {
          required: true,
        },
      } as QuestionOptions,
    ]

    it('returns true if all required subquestions are in answers', () => {
      const answers = {
        1: {
          id: 1,
          value: '1',
        } as Answer,
        2: {
          id: 2,
          value: '1',
        } as Answer,
        4: {
          id: 4,
          value: '1',
        } as Answer,
        10: {
          id: 10,
        } as Answer,
      }
      const result = areRequiredFilled(questions, answers)
      expect(result).toBe(true)
    })
    it('returns true if all required subquestions are in answers (2)', () => {
      const answers = {
        1: {
          id: 1,
          value: '1',
        } as Answer,
        2: {
          id: 2,
          value: '3',
        } as Answer,
        4: {
          id: 4,
          value: '1',
        } as Answer,
        11: {
          id: 11,
        } as Answer,
      }
      const result = areRequiredFilled(questions, answers)
      expect(result).toBe(true)
    })
    it('returns false if a subquestion is not in answers', () => {
      const answers = {
        1: {
          id: 1,
          value: '1',
        } as Answer,
        2: {
          id: 2,
          value: '3',
        } as Answer,
        4: {
          id: 4,
          value: '1',
        } as Answer,
      }
      const result = areRequiredFilled(questions, answers)
      expect(result).toBe(false)
    })
    it('returns true if a subquestion is not required', () => {
      const notRequiredQuestions = [
        {
          id: 2,
          validation: {
            required: true,
          },
          sub_questions: [
            {
              values: ['1', '2'],
              questions: [
                {
                  id: 10,
                  validation: {
                    required: false,
                  },
                },
              ],
            },
            {
              values: ['3', '4'],
              questions: [
                {
                  id: 11,
                  validation: {
                    required: false,
                  },
                },
              ],
            },
          ],
        } as QuestionOptions,
      ]
      const answers = {
        2: {
          id: 2,
          value: '3',
        } as Answer,
      }
      const result = areRequiredFilled(notRequiredQuestions, answers);
      expect(result).toBe(true)
    })
  })
})
