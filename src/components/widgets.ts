import RatingQuestion from '@/components/questions/RatingQuestion.vue'
import AgeQuestion from '@/components/questions/AgeQuestion.vue'
import EmailQuestion from '@/components/questions/EmailQuestion.vue'
import PasswordQuestion from '@/components/questions/PasswordQuestion.vue'
import CommentQuestion from '@/components/questions/CommentQuestion.vue'

export const widgets = {
  rating: RatingQuestion,
  age: AgeQuestion,
  email: EmailQuestion,
  password: PasswordQuestion,
  comment: CommentQuestion,
}

export type WidgetType = keyof typeof widgets;
