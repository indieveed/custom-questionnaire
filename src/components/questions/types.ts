import { WidgetType } from '../widgets'

export interface SubQuestionOptions {
  values: string[];
  // eslint-disable-next-line no-use-before-define
  questions: QuestionOptions[];
}
export interface QuestionOptions {
  id: number;
  type: WidgetType;
  label: string;
  validation: {
    required: boolean;
  };
  // eslint-disable-next-line camelcase
  sub_questions: SubQuestionOptions[];
}

export interface Answer {
  id: number;
  value?: string;
  errors?: string[];
}
